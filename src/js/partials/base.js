$(function() {

    $(window).scroll(function () {

        var windowScroll = $(window).scrollTop();

        if (windowScroll) {
            $('.header-menu_mod_scroll').addClass('header-menu_state_scroll');
            $('.header-menu_mod_static').addClass('header-menu_state_hide');
        } else {
            $('.header-menu_mod_scroll').removeClass('header-menu_state_scroll');
            $('.header-menu_mod_static').removeClass('header-menu_state_hide');
        }
    });

    $(document).on('mouseout', '.header-menu__item', function(e) {
        $(this).parents('.header-menu').find('.header-submenu').stop().fadeOut(300);
    });

    $(document).on('mouseover', '.header-menu__item', function(e) {
        $(this).parents('.header-menu').find('.header-submenu').stop().fadeIn(300);
    });

    var video = $('.popup-player__video')[0];

    $(document).on('click', '.main-video__play', function(e) {
        $('html').addClass('html_state_popup');
        $('.popup').fadeIn(1000);
        (video.paused) ? video.play() : video.pause();
    });

    $(document).on('click', '.popup-player__close', function(e) {
        $('html').removeClass('html_state_popup');
        $('.popup').fadeOut(1000);
        (video.paused) ? video.play() : video.pause();
    });

    $(document).on('click', '.main-calendar-list__item', function(e) {
        if( !$(this).hasClass('main-calendar-list__item_state_active') ) {

            var id = $(this).find('a').attr('data-id');

            $('.main-calendar__aside').attr('class', 'main-calendar__aside main-calendar__aside_mod_' + id);

            $('.main-calendar-list__item').removeClass('main-calendar-list__item_state_active');
            $('.main-calendar__item').removeClass('main-calendar__item_state_active');


            $(this).addClass('main-calendar-list__item_state_active');
            $('.main-calendar__item[data-id='+ id +']').addClass('main-calendar__item_state_active');
        }

        return false;
    });

    $(document).on('click', '.main-guest-tab__item', function(e) {
        $('.main-guest-tab__item').removeClass('main-guest-tab__item_state_active');
        $(this).addClass('main-guest-tab__item_state_active');

        $('.main-guest__village').toggle();
        $('.main-guest__forest').toggle();

        return false;
    });



    function kenSlider(timeout, pause) {
        var params = {};
        return {
            next: function() {
                this.choice(params.items.length - 1 > params.activeIndex ? params.activeIndex + 1 : 0);
            },
            prev: function() {
                this.choice(params.activeIndex > 0 ? params.activeIndex - 1 : params.items.length - 1);
            },
            choice: function(index) {

                params.items.eq(index).addClass(params.acticeClass);
                params.items.eq(params.activeIndex).removeClass(params.acticeClass);
                params.activeIndex = index;
            },
            autoToggle: function() {
                var _this = this;

                clearTimeout(params.timer);
                clearTimeout(params.userActivityTimer);

                params.userActivityTimer = setTimeout(function() {
                    _this.next();

                    params.timer = setInterval(function() {
                        _this.next();
                    },params.timeout);
                }, params.pause);
            },
            init: function($node) {
                var _this = this;

                params = {
                    node: $node,
                    items: $node.find('.main-cause__item'),
                    nextBtn: $node.find('.main-cause__next'),
                    prevBtn: $node.find('.main-cause__prev'),
                    acticeClass: 'main-cause__item_state_active',
                    autoplay: false,
                    timeout: timeout || 5000,
                    pause: pause || 5000,
                };

                if( params.autoplay && params.items.length > 1 ) {
                    params.timer = setInterval(function() {
                        _this.next();
                    },params.timeout)
                }

                params.activeIndex = params.items.index($('.' + params.acticeClass));
                params.activeIndex = params.activeIndex > -1 ? params.activeIndex : 0;

                params.nextBtn.on('click', function(e) {
                    if(params.autoplay)
                        _this.autoToggle();

                    _this.next();
                });
                params.prevBtn.on('click', function(e) {
                    if(params.autoplay)
                        _this.autoToggle();

                    _this.prev();
                });
            }
        }
    };

    var mainCause = kenSlider();
    mainCause.init($('.main-cause').eq(0));


    function guestSlider(timeout, pause) {
        var params = {};
        return {
            next: function() {
                this.choice(params.items.length - 1 > params.activeIndex ? params.activeIndex + 1 : 0);
            },
            prev: function() {
                this.choice(params.activeIndex > 0 ? params.activeIndex - 1 : params.items.length - 1);
            },
            choice: function(index) {

                params.items.eq(index).addClass(params.acticeClass);
                params.items.eq(params.activeIndex).removeClass(params.acticeClass);
                params.activeIndex = index;
            },
            autoToggle: function() {
                var _this = this;

                clearTimeout(params.timer);
                clearTimeout(params.userActivityTimer);

                params.userActivityTimer = setTimeout(function() {
                    _this.next();

                    params.timer = setInterval(function() {
                        _this.next();
                    },params.timeout);
                }, params.pause);
            },
            init: function($node) {
                var _this = this;

                params = {
                    node: $node,
                    items: $node.find('.main-guest__item'),
                    nextBtn: $node.find('.main-guest__next'),
                    prevBtn: $node.find('.main-guest__prev'),
                    acticeClass: 'main-guest__item_state_active',
                    autoplay: false,
                    timeout: timeout || 5000,
                    pause: pause || 5000,
                };

                if( params.autoplay && params.items.length > 1 ) {
                    params.timer = setInterval(function() {
                        _this.next();
                    },params.timeout)
                }

                params.activeIndex = params.items.index($('.' + params.acticeClass));
                params.activeIndex = params.activeIndex > -1 ? params.activeIndex : 0;

                params.nextBtn.on('click', function(e) {
                    if(params.autoplay)
                        _this.autoToggle();

                    _this.next();
                });
                params.prevBtn.on('click', function(e) {
                    if(params.autoplay)
                        _this.autoToggle();

                    _this.prev();
                });
            }
        }
    };

    var guestCause = guestSlider();
    guestCause.init($('.main-guest').eq(0));

    function reviewsSlider(timeout, pause) {
        var params = {};
        return {
            next: function() {
                this.choice(params.items.length - 1 > params.activeIndex ? params.activeIndex + 1 : 0);
            },
            prev: function() {
                this.choice(params.activeIndex > 0 ? params.activeIndex - 1 : params.items.length - 1);
            },
            choice: function(index) {

                params.items.eq(index).addClass(params.acticeClass);
                params.items.eq(params.activeIndex).removeClass(params.acticeClass);
                params.activeIndex = index;
            },
            autoToggle: function() {
                var _this = this;

                clearTimeout(params.timer);
                clearTimeout(params.userActivityTimer);

                params.userActivityTimer = setTimeout(function() {
                    _this.next();

                    params.timer = setInterval(function() {
                        _this.next();
                    },params.timeout);
                }, params.pause);
            },
            init: function($node) {
                var _this = this;

                params = {
                    node: $node,
                    items: $node.find('.main-reviews__item'),
                    nextBtn: $node.find('.main-reviews__next'),
                    prevBtn: $node.find('.main-reviews__prev'),
                    acticeClass: 'main-reviews__item_state_active',
                    autoplay: false,
                    timeout: timeout || 5000,
                    pause: pause || 5000,
                };

                if( params.autoplay && params.items.length > 1 ) {
                    params.timer = setInterval(function() {
                        _this.next();
                    },params.timeout)
                }

                params.activeIndex = params.items.index($('.' + params.acticeClass));
                params.activeIndex = params.activeIndex > -1 ? params.activeIndex : 0;

                params.nextBtn.on('click', function(e) {
                    if(params.autoplay)
                        _this.autoToggle();

                    _this.next();
                });
                params.prevBtn.on('click', function(e) {
                    if(params.autoplay)
                        _this.autoToggle();

                    _this.prev();
                });
            }
        }
    };

    var reviewsCause = reviewsSlider();
    reviewsCause.init($('.main-reviews').eq(0));
});
